# nexus-docs

## Nexus-koi???

Bienvenue dans le dépot de documentation du groupe **Nexus**.
Vous trouverez ici une librairie de documents extensible et propres à certains domaine de l'informatique comme :

+ Le développement web
+ Le développement logiciel
+ L'administration des systèmes, réseaux et sécurité informatique

Mais aussi :

+ Des guides de rédaction
+ Des guides protocolaires du groupe **Nexus**
+ Des guides divers et variés

... Et peut-être bien d'autres choses.

Notez que ce dépot est commun au groupe et reste libre d'utilisation.
